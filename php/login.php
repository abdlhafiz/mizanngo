
<?php

    require_once('config.php');

    if (!$db_sql_conn){

        echo 'DB Connection Failed'. mysqli_connect_error();
    
        die();
    }

    session_start();
    // If form submitted, insert values into the database.
    if (isset($_POST['username'])){

        $auth_status=false;
        $err_msg    ='';
        $ok_msg    ='';
            // removes backslashes
        $username = stripslashes($_POST['username']);
            //escapes special characters in a string
        $username = mysqli_real_escape_string($db_sql_conn,$username);
        $password = stripslashes($_POST['password']);
        $password = mysqli_real_escape_string($db_sql_conn,$password);

        //Checking is user existing in the database or not
            $query = "SELECT * FROM `members` WHERE email = '$username' and password='$password'";

        // Perform query
        if ($result = mysqli_query($db_sql_conn,$query)) {            
            $rows   = mysqli_num_rows($result);
                
            if($rows==1){
                $_SESSION['username']   = $username;
                $_SESSION['password']   = $password;
                $_SESSION['id']         = session_id();
                    // Redirect user to index.php
                $auth_status = true;
                $ok_msg = 'Authentication Sucessfull';
            }else{
                $err_msg .= "<strong>Incorrect Username or password. Please try again</strong>";
            }
        }else{

            $err_msg .= "<strong>Query Error: </strong></br>".$query.'</br>';//.mysqli_error($$db_sql_conn);
        }

        mysqli_close($db_sql_conn);

        if($auth_status){
            // echo session_id();

            $ok_msg .= "<script>location.href = 'index.html';</script>";
            echo json_encode(array('success'=>1, 'message'=> $ok_msg ));
            die();
        }else{

            
            echo json_encode(array('success'=>0, 'message'=> $err_msg ));
        }
        
    }else {
        /* not member or incorrect, please register if you are new , forgot password  */
        echo json_encode(array('success'=>0, 'message'=> 'Incorrect values, Please Try Again !' ));
    }
?>

