<?php

require_once "config.php";
if (!$db_sql_conn){

    echo 'DB Connection Failed'. mysqli_connect_error();

    die();
}

if(isset($_POST['donation_amount']) )
{

    function return_error($error)
    {
        echo json_encode(array('success'=>0, 'message'=>$error));
        die();
    }

    // check for empty required fields
    if (!isset($_POST['doner_fname']) ||
        !isset($_POST['doner_gfname']) ||
        !isset($_POST['doner_phone']) ||
        !isset($_POST['doner_email']) ||
        !isset($_POST['donation_amount']))
    {
        return_error('Please fill in all required fields.');
    }

    // form field values

    $donation_amount = $_POST['donation_amount']; // required
    $doner_email     = $_POST['doner_email']; // required
    $doner_phone    = $_POST['doner_phone']; // required
    $doner_fname    = $_POST['doner_fname']; // required
    $doner_gfname   = $_POST['doner_gfname']; // required
    $doner_address  = $_POST['doner_address'];
    $doner_comment  = $_POST['doner_comment'];

    // form validation

    $error_message = "";
    // donation amount must be positive number
    if($donation_amount <= 0){
        $this_error = 'Please enter a valid Donation Amount.';
        $error_message .= ($error_message == "") ? $this_error : "<br/>".$this_error;
    }
    // doner_fname gfname
    $doner_fname_exp = "/^[a-z0-9 .\-]+$/i";
    if (!preg_match($doner_fname_exp,$doner_fname))
    {
        $this_error = 'Please enter a valid First Name.';
        $error_message .= ($error_message == "") ? $this_error : "<br/>".$this_error;
    }    
    if (!preg_match($doner_fname_exp,$doner_gfname))
    {
        $this_error = 'Please enter a valid Last Name.';
        $error_message .= ($error_message == "") ? $this_error : "<br/>".$this_error;
    }     
    // valid email address
    $doner_email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
    if (!preg_match($doner_email_exp,$doner_email))
    {
        $this_error = 'Please enter a valid Email address.';
        $error_message .= ($error_message == "") ? $this_error : "<br/>".$this_error;
    } 


    // if there are validation errors
    if(strlen($error_message) > 0)
    {
        return_error($error_message);
    }
    $success_msg="";
    $crud_ops=false;
    // prepare CRUD Operation
    //  (id,donation_id,project_id,program_id,donation_amount,doner_name, doner_fname,doner_gfname,doner_email,doner_phone,doner_address,doner_comment,created_at,status)

            $donation_id = rand();
            $sql_stmnt = "INSERT INTO `donation` (`id`, `donation_id`, `project_id`, `program_id`, `donation_amount`, `doner_name`, `doner_fname`, `doner_gfname`, `doner_email`, `doner_phone`, `doner_address`, `doner_comment`, `created_at`, `donation_status`) 
                            VALUES (NULL,'$donation_id', NULL,NULL, '$donation_amount', '$doner_fname', NULL, '$doner_gfname', '$doner_email', '$doner_phone', '$doner_address', '$doner_comment', current_timestamp(), 'pending')";


            if (mysqli_query($db_sql_conn, $sql_stmnt)) {
                // $success_msg =  "New record created successfully";
                $crud_ops = true;
            } else {
                $error_message .= "Error: " . $sql_stmnt . "<br>" . mysqli_error($db_sql_conn);
            }
            
            mysqli_close($db_sql_conn);

    if ($crud_ops)
    {
        $success_msg .= "<h4>Your Donation Form is Submitted Successfully.</h4> <hr><ul>";
        $success_msg .="<li> Donation Amount: "."<strong>". $_POST['donation_amount']."</strong></li>" ;
        $success_msg .="<li> First Name: "."<strong>".$_POST['doner_fname']."</strong></li>";
        $success_msg .="<li> Last Name: ". "<strong>".$_POST['doner_gfname']."</strong></li>" ;
        $success_msg .="<li> Phone: ". "<strong>".$_POST['doner_phone']."</strong></li>";
        $success_msg .="<li> Email: ". "<strong>".$_POST['doner_email']."</strong></li>";
        // $success_msg .="<hr> <strong> ለትብብርዎ እጅግ እናመሰግናለን!! ቀጣይ በኢሜይሎ ወይም ስልኮ ደውለን እናሳውቆታለን!!.</strong>";

        $success_msg .="<hr><button type='Button' id='myButton' class='btn btn-primary' name='donateNow'> ለትብብርዎ እጅግ እናመሰግናለን!! ቀጣይ በኢሜይሎ ወይም ስልኮ ደውለን እናሳውቆታለን!!. </button>";

        $success_msg .="<script>document.ajax-form.reset();
                                document.getElementById('myButton').onclick = function () {
                                location.href = 'index.html';
                            };</script>";
        echo json_encode(array('success'=>1, 'message'=> $success_msg)); 
    }
    else 
    {
        echo json_encode(array('success'=>0, 'message'=>'An error occured. Please try again later. </br></br></br>'."ERR : ".$error_message)); 
        die();        
    }
}
else
{
    echo json_encode(array('success'=>0, 'message'=>'Please fill in all required fields.')); 
    // echo 'Please fill in all required fields.';
    die();
}
?>