<?php
session_start();

require_once "config.php";

if (!$db_sql_conn || mysqli_connect_errno()){

    echo json_encode(array('success'=>0, 'message'=> "SQL DB Error: " . mysqli_connect_error()));
    die();
}
//  name, fname, gfname, address,city, country, phone1, phone2, email, pwd, repeat_pwd,donation,


if(isset($_POST['name']))
{
    function return_error($error)
    {
        // $_SESSION['error_message'] = $error;
        // echo header('Location', 'error.php');
        echo json_encode(array('success'=>0, 'message'=> $error)); // 
        die();
    }

    // check for empty required fields
    if (!isset($_POST['name']) ||
        !isset($_POST['fname']) ||
        !isset($_POST['gfname']) ||
        !isset($_POST['address']) ||
        !isset($_POST['city']) ||
        !isset($_POST['country']) ||
        !isset($_POST['phone1'])||
        !isset($_POST['email']) ||
        !isset($_POST['pwd']) || 
        !isset($_POST['repeat_pwd']))
    {
        return_error('Please fill in all required fields.');
    }

    // form field values

    $name = $_POST['name']; // required
    $fname     = $_POST['fname']; // required
    $gfname    = $_POST['gfname']; // required
    $address    = $_POST['address']; // required
    $city   = $_POST['city']; // required
    $country  = $_POST['country'];
    $phone1  = $_POST['phone1'];
    $phone2  = $_POST['phone2'];
    $email  = $_POST['email'];
    $sex = $_POST['sex'];
    $DOB = $_POST['DOB'];

    // $pwd            = $_POST['pwd'];
    // $repeat_pwd     = $_POST['repeat_pwd'];
    // $donation       = $_POST['donation'];

    // form validation
    $success_msg        = '';
    $error_message      = '';

    // Password Mismatch 
    // if ($_POST['pwd'] !== $_POST['repeat_pwd'])
    // {
    //     $error_message .="Password did not match! Try again. ";
    // }
    // donation amount must be positive number
    // if($donation <= 0){
    //     $this_error = 'Please enter a valid Donation Amount.';
    //     $error_message .= ($error_message == "") ? $this_error : "<br>".$this_error;
    // }
    // doner_fname gfname
    $fname_exp = "/^[a-z0-9 .\-]+$/i";
    if (!preg_match($fname_exp,$name))
    {
        $this_error = "Please enter a valid First Name.";
        $error_message .= ($error_message == "") ? $this_error : "<br>".$this_error;
    }   
    if (!preg_match($fname_exp,$fname))
    {
        $this_error = "Please enter a valid Father Name.";
        $error_message .= ($error_message == "") ? $this_error : "<br>".$this_error;
    }   
    if (!preg_match($fname_exp,$gfname))
    {
        $this_error = "Please enter a valid Last Name.";
        $error_message .= ($error_message == "") ? $this_error : "<br>".$this_error;
    }     
    // valid email address
    $email_exp = "/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/";
    if (!preg_match($email_exp,$email))
    {
        $this_error = "Please enter a valid Email address.";
        $error_message .= ($error_message == "") ? $this_error : "<br>".$this_error;
    } 

    // if there are validation errors
    if(strlen($error_message) > 0)
    {
        return_error($error_message);
    }

    $crud_ops=false;

    // prepare CRUD Operation
    $user_id = rand(); $member_id = rand(1001,11000);

    $_SESSION['member_id'] = $member_id;

    $sql_stmnt = "INSERT INTO `members` (`id`, `user_id`, `member_id`, `name`, `fname`, `gfname`, `DOB`, `sex`, `picture`, `address`, `city`, `country`, `email`, `phone1`, `phone2`, `role`, `username`, `password`, `created_at`, `member_status`) 
    VALUES (NULL, '$user_id', '$member_id', '$name', '$fname', '$gfname', '$DOB', '$sex', NULL, '$address', '$city', '$country', '$email', '$phone1', '$phone2', 'member', '$email', '1324', current_timestamp(), 'pending');";

    if (mysqli_query($db_sql_conn, $sql_stmnt)) {
        $success_msg .=  "New member Registerd successfully";
        $crud_ops = true;
    } else {

        $error_message .=  mysqli_error($db_sql_conn); //$sql_stmnt .
    }

    mysqli_close($db_sql_conn);

    if ($crud_ops)// && (!strlen($error_message) > 0))
    {
        // $data = [ 'member_id' => $member_id, 'full_name' => $name . ' '.$fname. ' '. $gfname, 'DOB' => $_POST['DOB'], 'Gender' => $_POST['sex'] ];
        $success_msg .= "<h4>Registration Successfull.<br> <strong> Waiting for Adminstrator Approval</strong> </h4> ";

        $success_msg .="<ul><li> Name: "."<strong>". $_POST['name']."</strong></li>" ;
        $success_msg .="<li> Father Name: "."<strong>".$_POST['fname']."</strong></li>";
        $success_msg .="<li> Grand Father Name: ". "<strong>".$_POST['gfname']."</strong></li>" ;

        $success_msg .="<li> Date of Birth: ". "<strong>".$_POST['DOB']."</strong></li>" ;
        $success_msg .="<li> Sex: ". "<strong>".$_POST['sex']."</strong></li>" ;

        $success_msg .="<li> Address: ". "<strong>".$_POST['address']."</strong></li>" ;
        $success_msg .="<li> City: ". "<strong>".$_POST['city']."</strong></li>" ;
        $success_msg .="<li> Country: ". "<strong>".$_POST['country']."</strong></li>" ;
        $success_msg .="<li> Phone1: ". "<strong>".$_POST['phone1']."</strong></li>";
        $success_msg .="<li> Email: ". "<strong>".$_POST['email']."</strong></li></ul>";
        
        $success_msg .="<hr>የማህበራችን አባል በመሆንዎ እናመሰግናለን!! ቀጣይ በኢሜይሎ ወይም ስልኮ ደውለን እናሳውቆታለን!!.";
        $success_msg .="<hr><button type='Button' id='myButton' class='btn btn-primary' name='donateNow'> OK </button>";

        $success_msg .="<script>
                            document.getElementById('input_data_form').style.display = 'none';
                            
                            document.getElementById('myButton').onclick = function(){
                                    location.href = 'index.html';
                                }
                        </script>";

        echo json_encode(array('success'=>1, 'message'=> $success_msg)); 
        die();
    }
    else 
    {
        // echo header('Location', 'error.php');
        echo json_encode(array('success'=>0, 'message'=> $error_message)); 
        die();        
    }
}
else
{
    echo json_encode(array('success'=>0, 'message'=>'Please fill in all required fields.')); 
    // echo 'Please fill in all required fields.';
    die();
}

?>

