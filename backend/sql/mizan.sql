-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2017 at 10:53 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mizan`
--
-- CREATE DATABASE 'mizan';
-- at 000webhost.com --
-- db name: mizanDB
-- db_user name: mizandb_hafiz or id17650402_mizandb_hafiz
-- db_user password: #Ahme@0910%#@)
-- #Ahme@0910%#@)
--
-- --------------------------------------------------------
--
-- Table structure for table `admin_accounts`
--

CREATE TABLE `admin_accounts` (
  `id` int(25) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `series_id` varchar(60) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `admin_type` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_accounts`
--

INSERT INTO `admin_accounts` (`id`, `user_name`, `password`, `series_id`, `remember_token`, `expires`, `admin_type`) VALUES
(4, 'superadmin', '$2y$10$eo7.w0Ttuy8mOBMvDlGqDeewQERkXu//7qO3jXp5NC76LwfAZpNrO', 'rvuWJHMd5LTxLC2J', '$2y$10$LDUi4w/UAM2PgfMoKkLo4.igJX39G5/WQOEDHRaDy3y2KZeIxXggm', '2019-02-16 22:39:57', 'super'),
(7, 'anand', '$2y$10$OrQFRZdSUP3X2kvGZrg.zeplQkxcJAq1s6atRehyCpbEvOVPu8KPe', NULL, NULL, NULL, 'admin'),
(8, 'admin', '$2y$10$RnDwpen5c8.gtZLaxHEHDOKWY77t/20A4RRkWBsjlPuu7Wmy0HyBu', 'MyG5Xw2I12EWdJeD', '$2y$10$XL/RhpCz.uQoWE1xV77Wje4I4ker.gtg7YV4yqNwLZfzIYnP7E8Na', '2019-08-22 01:12:33', 'admin');


--
-- Indexes for table `admin_accounts`
--
ALTER TABLE `admin_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name` (`user_name`);

--
-- AUTO_INCREMENT for table `admin_accounts`
--
ALTER TABLE `admin_accounts`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `user_id` varchar(16) NOT NULL UNIQUE,
  `member_id` varchar(16) NOT NULL UNIQUE,

  `name` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `gfname` varchar(100) NOT NULL,
  `DOB` varchar(100),
  `sex` varchar(100),
  `picture` varchar(100) ,

  `address` varchar(100),
  `city` varchar(100) ,
  `country` varchar(100) ,
  `email` varchar(100) NOT NULL UNIQUE,
  `phone1` varchar(100) NOT NULL,
  `phone2` varchar(100),

  `role` varchar(100) NOT NULL, /* admin, member, guest*/
  `username` varchar(100) NOT NULL UNIQUE,
  `password` varchar(100) NOT NULL,

  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `member_status` varchar(16)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `members`
--

-- INSERT INTO `members` (`id`, `user_id`,`member_id`,`name`,`fname`,`gfname`,`DOB`,`sex`,`picture`, `address`, `city`,`country`,`email`,`phone`,`role`,`username`,`password`,`created_at`,`status`) VALUES
-- (1,`123456`,`987654`,`Hafiz`,`Assefa`,`Oumer`,`07/16/1987`,`Male`,``,`Jemo`,`Addis Ababa`,`Ethiopia`,`abdlhafiz@gmail.com`,`+251910605031`,`admin`,`hafizo`,`hafiz123`,``,``),
-- (2,``,``,``,``,``,``,``,``,``,``,``,``,``,``,``,``,``,``);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

--
-- Table structure for table `newsfeed`
--
CREATE TABLE IF NOT EXISTS `newsfeed` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `newsfeed_id` varchar(16) NOT NULL UNIQUE,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  `author_id` varchar(16) NOT NULL,
  `media_id` varchar(16) , /* array of pictures or video file url */
  `title` varchar(256) NOT NULL, 
  `sub_title` varchar(256) ,
  `content` varchar(4096) NOT NULL,
  `type` varchar(100) NOT NULL,     /* news, comming soon, projects, compaign, Training, ስለ እኛ, vission, mission, misclaneous */

  `view_count` varchar(100),
  `like_count` varchar(100) ,
  `comments_id` varchar(16) , /* array of comments given */

  `newsfeed_status` varchar(16)

-- 'id','newsfeed_id','created_at','author_id','media_id','title','sub-title','content','type','view_count','like_count','comments_id','newsfeed_status'


) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;
--
-- Dumping data for table `newsfeed`
--
-- INSERT INTO `newsfeed` (`id`, `newsfeed_id`,`created_at`,`member_id`,`media_id`,`title`,`description`,`type`,`view_count`,`like_count`, `comments_id`, `status`) VALUES
-- (1,``,``,``,``,``,``,``,``,``,``,``),
-- (1,``,``,``,``,``,``,``,``,``,``,``),
-- (1,``,``,``,``,``,``,``,``,``,``,``),
-- (1,``,``,``,``,``,``,``,``,``,``,``);

--
-- Table structure for table `messages`
--
CREATE TABLE IF NOT EXISTS `messages` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `message_id` varchar(16) NOT NULL UNIQUE,

  `newsfeed_id` varchar(16) ,

  `user_email` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL, /* array of pictures or video files */
  `user_phone` varchar(100),
  `message` varchar(100) NOT NULL,

  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `message_status` varchar(16)     /**/
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;
--
-- Dumping data for table `messages`
--
-- INSERT INTO `messages` (`id`, `message_id`,`newsfeed_id`,`created_at`,`user_email`,`user_name`,`user_phone`,`message`, `status`) VALUES
-- (1,``,``,``,``,``,``,``,``);
--
-- Table structure for table `media`
--
CREATE TABLE IF NOT EXISTS `media` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,

  `media_id` varchar(16) NOT NULL UNIQUE,

  `catagory_id` varchar(16),  /* user_id, member_id, newsfeed_id, or other*/

  `catagory` varchar(100) NOT NULL, /*projects, training, profiles, */
  `type` varchar(100) ,     /*picture, video*/
  `url` varchar(100) NOT NULL,

  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `media_status` varchar(16)

-- 'id','media_id', 'catagory_id', 'catagory', 'type','url', 'created_at', 'media_status'

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;
--
-- Dumping data for table `media`
--
-- INSERT INTO `media` (`id`, `media_id`,`newsfeed_id`,`type`,`url`,`created_at`,`status`) VALUES
-- (1,``,``,``,``,``,``);

--
-- Table structure for table `donation`
--
CREATE TABLE IF NOT EXISTS `donation` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `donation_id` varchar(16) NOT NULL UNIQUE,

  `project_id` varchar(16), /* the donation may be for specific project*/
  `program_id` varchar(16), /* the donation may be for specific program - which contains multiple projects*/

  `donation_amount` int(11) NOT NULL,

  `doner_name` varchar(100) NOT NULL, 
  `doner_fname` varchar(100) , 
  `doner_gfname` varchar(100) NOT NULL,

  `doner_email` varchar(100) NOT NULL, 
  `doner_phone` varchar(100) NOT NULL, 
  `doner_address` varchar(100) , 

  `doner_comment` varchar(100), 

  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `donation_status` varchar(16)
--  id,donation_id,project_id,program_id,donation_amount,doner_name, doner_fname,doner_gfname,doner_email,doner_phone,doner_address,doner_comment,created_at,status

) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;
--
-- Dumping data for table `donation`
--
-- INSERT INTO `donation` (`id`, `donation_id`,`newsfeed_id`,`type`,`url`,`created_at`,`status`) VALUES
-- (1,``,``,``,``,``,``);

--
-- Table structure for table `project`
--
CREATE TABLE IF NOT EXISTS `project` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `project_id` varchar(16) NOT NULL UNIQUE,

  `program_id` varchar(16), /* the project may belongto a program - which contains multiple projects*/

  `project_name` varchar(100) NOT NULL,
  `project_overview` varchar(100) NOT NULL,
  `project_goals` varchar(100) NOT NULL,
  `project_description` varchar(100) NOT NULL,
  `project_milestones` varchar(100) NOT NULL,

  `project_location` varchar(100) NOT NULL,
  `project_budget` int(11) NOT NULL,
  `project_start_date` varchar(100) NOT NULL,
  `project_end_date` varchar(100) NOT NULL,
  `project_progress` int(100) NOT NULL,

  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `project_status` varchar(16)


) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;
--
-- Dumping data for table `project`
--
-- INSERT INTO `project` (`id`, `project_id`,`newsfeed_id`,`type`,`url`,`created_at`,`status`) VALUES
-- (1,``,``,``,``,``,``);

--
-- Table structure for table `program`
--
CREATE TABLE IF NOT EXISTS `program` (
  `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `program_id` varchar(16) NOT NULL UNIQUE,
  `project_id` varchar(16), /*array of project ids*/

  `program_name` varchar(100) NOT NULL,
  `program_overview` varchar(100) NOT NULL,
  `program_goals` varchar(100) NOT NULL,
  `program_description` varchar(250) NOT NULL,
  `program_milestones` varchar(100) NOT NULL,

  `program_location` varchar(100) NOT NULL,
  `program_budget` int(11) NOT NULL,
  `program_start_date` TIMESTAMP,
  `program_end_date` TIMESTAMP,
  `program_progress` int(100) NOT NULL,

  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `program_status` varchar(16)


) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;
--
-- Dumping data for table `program`
--
-- INSERT INTO `program` (`id`, `program_id`,`newsfeed_id`,`type`,`url`,`created_at`,`status`) VALUES
-- (1,``,``,``,``,``,``);


-- pageview

CREATE TABLE `pageview` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `page` text NOT NULL,
 `userip` text NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1


-- totalview

CREATE TABLE `totalview` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `page` text NOT NULL,
 `totalvisit` text NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1