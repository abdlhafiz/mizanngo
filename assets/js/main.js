

$(function(){

	/*  Gallery lightBox
 	================================================*/ 

 	if( $(".lightbox").length > 0 ) {

		$(".lightbox").prettyPhoto();
	}

	/*  Owl carousel
 	================================================*/ 

 	if( $(".owl-carousel").length > 0 ) {

		$(".owl-carousel").owlCarousel({

			 margin:25,
			 stagePadding: 25,
	   		 nav:true,
	   		 navText: [
		      "<i class='glyphicon glyphicon-chevron-left'></i>",
		      "<i class='glyphicon glyphicon-chevron-right'></i>"
		    ],
		    responsive:{

		        0:{
		            items:2
		        },
		        600:{
		            items:4
		        },
		        1000:{
		            items:8
		        }
		    }

		});
	}


	 /* Contact form ajax Handler
    ================================================*/

    $(".ajax-form").on('submit', function() {
    	
    	var form = $(this);
        var formURL = $(this).attr("action");
        var postData = $(this).serializeArray();

		// $('#donateModal').modal('show');

        $.ajax({

            url: formURL,
            type: 'POST',
            data: postData,
            dataType: 'json',
			

			beforeSend: function() {
				document.getElementById('div_loader').style.display = 'initial';
				// form.find("#div_loader").show();
			 },
			complete: function(){
				document.getElementById('div_loader').style.display = 'none';
				// form.find("#div_loader").hide();
			 },

            success:function(data, textStatus, jqXHR){

                if(data.success==1){

					// form.style.display = 'initial';	
					// form.style.display = 'visible';

					document.getElementById('profile-picture').style.display = 'initial';

					form.find(".alert").fadeOut();
                    form.find(".alert-success").html(data.message);
                    form.find(".alert-success").fadeIn(600); 

                }else{

                	form.find(".alert").fadeOut();
                    form.find(".alert-danger").html(data.message);
                    form.find(".alert-danger").fadeIn(600);
                }
            },

            // error: function(jqXHR, textStatus, errorThrown)  { 
			// 	alert(jqXHR.responseText);
			// 	// alert(thrownError);

            //     // console.log(errorThrown);
            // }, 


			error: function (jqXHR, exception) {
				var msg = '';
				if (jqXHR.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (jqXHR.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (jqXHR.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'parsererror') {
					msg = 'Requested JSON parse failed.';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + jqXHR.responseText;
				}
				// $('#post').html(msg);

				alert(jqXHR.responseText);
			},




        });
        
        return false;
     })


	//  $(".ajax-image-form").on('submit', function() {
    	
    // 	var form = $(this);
    //     var formURL = $(this).attr("action");
    //     var postData = $(this).serializeArray();

	// 	$.ajax({
	// 		xhr: function () {
	// 		  let xhr = new XMLHttpRequest();

	// 		  $('#progress_bar').style.display = 'block';

	// 		  xhr.upload.addEventListener("progress", function (e) {
	// 			if (e.lengthComputable) {
	// 			  let progress = Math.round(e.loaded * 100 / e.total);
	// 			  $("#progress_bar").css("width", progress + "%");
	// 			}
	// 		  }, false);
	// 		  return xhr;
	// 		},

	// 		type:'POST',
	// 		url: formURL,
	// 		data:formData,
	// 		cache:false,
	// 		contentType: false,
	// 		processData: false,
	// 		dataType: "json",
	// 		success:function(data){
	// 			$('uploaded_image').innerHTML = data; 
	// 		},
	// 		error: function(data){
	// 			$('uploaded_image').innerHTML = data; 
	// 		}
	// 	  });
        
    //     return false;
    //  })

    /*
	On scroll animations
	================================================
	*/


    var $elems = $('.animate-onscroll');

    var winheight = $(window).height();
    var fullheight = $(document).height();
 
    $(window).scroll(function(){
        animate_elems();
    });



    function animate_elems() {

	    wintop = $(window).scrollTop(); // calculate distance from top of window
	 
	    // loop through each item to check when it animates
	    $elems.each(function(){
	    	
	      $elm = $(this);
	 
	      if($elm.hasClass('animated')) { return true; } // if already animated skip to the next item
	 
	      topcoords = $elm.offset().top; // element's distance from top of page in pixels
	 
	      if(wintop > (topcoords - (winheight*.75))) {
	        // animate when top of the window is 3/4 above the element
	        $elm.addClass('animated');
	      }

	    });

	  } // end animate_elems()

	


 	/*  Google map Script
 	====================================================*/ 

	// function initMap() {

  		
  	// 	var mapLatitude = 31.423308 ; // Google map latitude 
  	// 	var mapLongitude = -8.075145 ; // Google map Longitude  

	//     var myLatlng = new google.maps.LatLng( mapLatitude, mapLongitude );

	//     var mapOptions = {

	//             center: myLatlng,
	//             mapTypeId: google.maps.MapTypeId.ROADMAP,
	//             zoom: 10,
	//             scrollwheel: false
	//           };   

	//     var map = new google.maps.Map(document.getElementById("contact-map"), mapOptions);

	//     var marker = new google.maps.Marker({
	    	
	//       position: myLatlng,
	//       map : map,
	      
	//     });

	//     // To add the marker to the map, call setMap();
	//     marker.setMap(map);

	//     // Map Custom style
	//     var styles = [
	// 	  {
	// 	    stylers: [
	// 	      { hue: "#1f76bd" },
	// 	      { saturation: 80 }
	// 	    ]
	// 	  },{
	// 	    featureType: "road",
	// 	    elementType: "geometry",
	// 	    stylers: [
	// 	      { lightness: 80 },
	// 	      { visibility: "simplified" }
	// 	    ]
	// 	  },{
	// 	    featureType: "road",
	// 	    elementType: "labels",
	// 	    stylers: [
	// 	      { visibility: "off" }
	// 	    ]
	// 	  }
	// 	];

	// 	map.setOptions({styles: styles});

	// };

	// if( $("#contact-map").length > 0 ) {

	// 	initMap();
		
	// }

});



		
