


    <header class="main-header">
        
    
        <nav class="navbar navbar-static-top">

            <div class="navbar-top">

              <div class="container">
                  <div class="row">

                    <div class="col-sm-6 col-xs-12">

                        <ul class="list-unstyled list-inline header-contact">
                            <li> <i class="fa fa-phone"></i> <a href="tel:">+251 11 884 7157</a></li>

                            <li> <i class="fa fa-envelope"></i> <a href="mailto:contact@mizan.org.et">contact@mizan.org.et</a> </li>
                       </ul> <!-- /.header-contact  -->
                      
                    </div>

                    <div class="col-sm-6 col-xs-12 text-right">

                        <ul class="list-unstyled list-inline header-social">

                            <li> <a href="https://www.facebook.com/%E1%88%9A%E1%8B%9B%E1%8A%95-%E1%8B%A8%E1%88%B0%E1%88%8B%E1%88%9D%E1%8A%93-%E1%88%8D%E1%88%9B%E1%89%B5-%E1%88%9B%E1%88%85%E1%89%A0%E1%88%AD-Mizan-Peace-And-Development-Association-101959658430306/?ref=page_internal" target="_blank"> <i class="fa fa-facebook"></i> </a> </li>
                            <li> <a href="#" target="_blank"> <i class="fa fa-twitter"></i>  </a> </li>
                            <!-- <li> <a href="#" target="_blank"> <i class="fa fa-google"></i>  </a> </li> -->
                            <li> <a href="#" target="_blank"> <i class="fa fa-youtube"></i>  </a> </li>
                            <li> <a href="https://t.me/mizanassociation" target="_blank"> <i class="fa fa-telegram"></i>  </a> </li>
                       </ul> <!-- /.header-social  -->
                      
                    </div>


                  </div>
              </div>

            </div>

            <div class="navbar-main">
              
              <div class="container">

                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>

                  </button>
                  
                  <a class="navbar-brand" href="index.html">
                    <img src="assets/images/mizan_logo.jpg"  style= "width: 80px; height:80px; border-radius: 50%; "  alt="ሚዛን በጎ አድራጎት"></a>
                  
                </div>

                <div id="navbar" class="navbar-collapse collapse pull-right">

                  <ul class="nav navbar-nav">
                    <!-- <li><a href="donation.html" class="btn btn-bg btn-primary btn-danger hidden-xl bounceInUp animated slow">እጅዎን ይዘርጉ</a></li> -->

                    

					<li><a href="index.html">መነሻ</a></li>
                    <li><a class="is-active" href="about.html">ስለ እኛ</a></li>
                    <li><a href="causes.html">ግቦቻችን</a> </li>
                    <li><a href="gallery.html">ፎቶዎች</a></li>
                    <li><a href="newsfeed.html">መረጃዎች</a></li>
                    <li><a href="registration.html">ይቀላቀሉን</a></li>
                  </ul>

                </div> <!-- /#navbar -->

              </div> <!-- /.container -->
              
            </div> <!-- /.navbar-main -->


        </nav> 

    </header> <!-- /. main-header -->

