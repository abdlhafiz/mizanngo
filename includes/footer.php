

    <footer class="main-footer">

<div class="footer-top">
    
</div>


<div class="footer-main">
    <div class="container">
        
        <div class="row">
            <div class="col-md-4">

                <div class="footer-col">

                    <h4 class="footer-title">ስለ እኛ <span class="title-under"></span></h4>

                    <div class="footer-content">
                        <p>
                            <strong>ሚዛን </strong> የሰላምና ልማት ማህበር ለማህበረሰባቸው አዎንታዊ አስተዋፅኦ ለማበርከት ዓላማ በሰነቁ ጥቂት ወጣቶች አነሳሽነት <strong></strong><strong> <a href="https://www.abyssinialaw.com/uploads/1113.pdf"> በአዋጅ ቁጥር 1113/2011</a> </strong> </strong>መሰረት ከኢፌዲሪ ሲቪል ማህበረሰብ ድርጅቶች ኤጀንሲ ፍቃድ አግኝቶ ጥቅምት 2012 የተመሰረተ መንግስታዊ ያልሆነ ሀገር በቀል ግብረሰናይ ድርጅት ነው፡፡
                        </p> 

                        <p>
                          ሚዛን በማህበራዊ ጉዳዮች ላይ የሚሰራ በጎ አድራጎት ማህበር ነው፡፡
                        </p>

                    </div>
                    
                </div>

                <div class="contact-items">

                    <ul class="list-unstyled contact-items-list">
                        <li class="contact-item" href="#"> <span class="contact-icon"> <i class="fa fa-map-marker"></i></span>ወልቂጤ ንብ ባንክ ሁለተኛ ፎቅ ቢሮ ቁጥር 109, ወልቂጤ, ኢትዮጵያ</li>
                    </ul>
                </div>

            </div>

            <div class="col-md-4">

                <div class="footer-col">

                    <h4 class="footer-title">መረጃዎች <span class="title-under"></span></h4>

                    <div class="footer-content">
                        <ul class="tweets list-unstyled">
                            <li class="tweet"> 

                                    ሚዛን የሰላምና ልማት ማህበር <strong><a href="https://www.muslimaid.org/">ከሙስሊም ኤይድ</a></strong> ዩ,ኤስ,ኤ ጋር በመተባበር በጉራጌ ዞን በገደባኖ ጉታዘር ወለኔ ወረዳ ከ5 ነጥብ 3 ሚሊዮን ብር በላይ በሆነ ወጪ የመማሪያ ክፍሎች የማስፋፊያ ግንባታ ፕሮጀክት ለማከናወን ሰኞ 15/06/2013 የመንግስት ባለድርሻ አካላት፣ የሚዛን ማህበር አመራርና የወረዳው ማህበረሰብ በተገኙበት የመሠረተ ድንጋይ የማኖር ስነ-ስርዓት ተከናውኗል። 

                            </li>


                        </ul>
                    </div>
                    
                </div>

            </div>


            <div class="col-md-4">

                <div class="footer-col">

                    <h4 class="footer-title">እኛን ለማግኘት <span class="title-under"></span></h4>

                    <div class="footer-content">

                        <div class="footer-form" >
                            
                            <form action="php/mail.php" class="ajax-form">

                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="ስም" required>
                                </div>

                                 <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="ኢሜይል" required>
                                </div>

                                <div class="form-group">
                                    <textarea name="message" class="form-control" placeholder="መእልክቶት" required></textarea>
                                </div>

                                <div class="form-group alerts">
                
                                    <div class="alert alert-success" role="alert">
                                      
                                    </div>

                                    <div class="alert alert-danger" role="alert">
                                      
                                    </div>
                                    
                                </div>

                                 <div class="form-group">
                                    <button type="submit" class="btn btn-submit pull-right">መልእክቱን ላክ</button>
                                </div>
                                
                            </form>

                        </div>
                    </div>
                    
                </div>

            </div>
            <div class="clearfix"></div>



        </div>
        
        
    </div>

<!-- <div id="googleMap" style="width:100%;height:400px;"></div> -->
</div>

<div class="footer-bottom">

    <div class="container text-right">
        @ copyrights 2021
        <!-- Sadaka @ copyrights 2015 - by <a href="http://www.farouk.pw/fr" target="_blank">Freelance web developer</a> -->
    </div>
</div>

</footer>



<!-- <script>
    function myMap() {
    var mapProp= {
    center:new google.maps.LatLng(51.508742,-0.120850),
    zoom:5,
    };
    var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
    }
</script> -->

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY&callback=myMap"></script> -->



<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')</script>

<!-- Bootsrap javascript file -->
<script src="assets/js/bootstrap.min.js"></script>


<!-- Template main javascript -->
<script src="assets/js/main.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<!-- <script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
</script> -->

<script>
    $(document).ready(function(){
        

        // $('li#admin-area').css('vis', 'true');

        // $('input').focus(function(){ $(this).css('background', '#f4f4');});
        // $('input').blur(function(){ $(this).css('background', 'white'); });
                        
    });	
</script>